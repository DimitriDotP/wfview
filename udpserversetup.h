#ifndef UDPSERVERSETUP_H
#define UDPSERVERSETUP_H

#include <QDialog>
#include <QComboBox>
#include <QList>
#include <QAudioDeviceInfo>

#include <QDebug>


struct SERVERUSER {
    QString username;
    QString password;
    quint8 userType;
};

struct SERVERCONFIG {
    bool enabled;
    bool lan;
    quint16 controlPort;
    quint16 civPort;
    quint16 audioPort;
    QString audioOutput;
    QString audioInput;
    QAudioDeviceInfo inputDevice;
    QAudioDeviceInfo outputDevice;
    quint8 resampleQuality;
    quint32 baudRate;

    QList <SERVERUSER> users;
};

namespace Ui {
    class udpServerSetup;
}

class udpServerSetup : public QDialog
{
    Q_OBJECT

public:
    explicit udpServerSetup(QWidget* parent = 0);
    ~udpServerSetup();

private slots:
    void on_usersTable_cellClicked(int row, int col);

public slots:    
    void receiveServerConfig(SERVERCONFIG conf);

signals:
    void serverConfig(SERVERCONFIG conf, bool store);

private:
    Ui::udpServerSetup* ui;
    void accept();
    QList<QComboBox*> userTypes;
};

#endif // UDPSERVER_H
